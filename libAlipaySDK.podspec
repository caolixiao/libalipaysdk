
Pod::Spec.new do |s|

  s.name         = "libAlipaySDK"
  s.version      = "15.4.1"
  s.summary      = "AlipaySDK -> v15.4.1  url -> https://docs.open.alipay.com/204/105295/"

  s.description  = "支付宝SDK"

  s.homepage     = "https://bitbucket.org/caolixiao/libalipaysdk"
  s.license      = "MIT"
  s.author       = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.ios.deployment_target = "8.0"

  s.source   = { :git => "https://caolixiao@bitbucket.org/caolixiao/libalipaysdk.git", :tag => "#{s.version}" }

  s.resource = "iOSDemo_2.0(SDK_15.4.1)/AlipaySDK.bundle"
  s.vendored_frameworks = "iOSDemo_2.0(SDK_15.4.1)/AlipaySDK.framework"

  s.frameworks = "SystemConfiguration", "CoreTelephony", "QuartzCore", "CoreText", "CoreGraphics", "CFNetwork", "CoreMotion"
  s.libraries = "c++", "z"

  s.requires_arc = true

end
